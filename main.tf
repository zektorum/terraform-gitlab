terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.111.0"
    }
  }
}

provider "yandex" {
  zone = "ru-central1-a"
}

variable "ssh_key" {
  type = string
}

resource "yandex_compute_instance" "gitlab" {
  name     = "gitlab-vm"
  hostname = "gitlab"

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      name     = "gitlab-drive"
      size     = 20
      type     = "network-ssd"
      image_id = "fd83s8u085j3mq231ago"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.default-subnet.id
    nat       = true
  }

  metadata = {
    ssh-keys  = "ubuntu:${var.ssh_key}"
    user-data = "${file("./configs/init.yaml")}"
  }
}

resource "yandex_vpc_network" "default-network" {
  name = "gitlab-network"
}

resource "yandex_vpc_subnet" "default-subnet" {
  name           = "gitlab-subnet"
  network_id     = yandex_vpc_network.default-network.id
  zone           = "ru-central1-a"
  v4_cidr_blocks = ["10.5.0.0/16"]
}

